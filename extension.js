var hx = require("hbuilderx");
const {
	previewMarkdown,
	showView
} = require("./src");
//该方法将在插件激活的时候调用
function activate(context) {
	let disposable = hx.commands.registerCommand('extension.helloWorld', () => {
		hx.window.showInformationMessage('你好，这是我的第一个插件扩展。');
	});
	let webviewPanel = hx.window.createWebView("extension.kux.mdp.view.markdown", {
		enableScripts: true
	});
	previewMarkdown(webviewPanel, context);
	//订阅销毁钩子，插件禁用的时候，自动注销该command。
	context.subscriptions.push(disposable);
	let previewGithub = hx.commands.registerCommand('extension.kux.mdp.github', () => {
		showView();
	});
	context.subscriptions.push(previewGithub);
	let onDidChangeTextDocumentEventDispose = hx.workspace.onDidChangeTextDocument(function(event) {
		let document = event.document;
		previewMarkdown(webviewPanel, context, document);
	});
	context.subscriptions.push(onDidChangeTextDocumentEventDispose);
	let onDidOpenTextDocumentEventDispose = hx.workspace.onDidOpenTextDocument(function(document) {
		previewMarkdown(webviewPanel, context, document);
	});
}
//该方法将在插件禁用的时候调用（目前是在插件卸载的时候触发）
function deactivate() {

}
module.exports = {
	activate,
	deactivate
}