﻿# kux-mdp-hxp
一个非常简单高效的 `markdown` 预览插件。

## 插件特色
+ 支持代码高亮
+ 支持修改后实时更新
+ 支持通过默认浏览器打开文档链接
+ 支持新建文档实时切换
+ ...

## 使用方法
打开一个 `markdown` 文件后右键点击 `kux markdown预览` 即可在右边打开一个新的预览视图。

> **提示**
> 预览视图依赖内置浏览器插件。


----
### 结语
kux不生产代码，只做代码的搬运工，欢迎各位大佬在插件市场搜索 `kux` 使用生态其他插件。