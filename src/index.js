var hx = require("hbuilderx");
let fs = require('fs');
const {
	markdown2html,
	getDocumentContentLineCount
} = require('./utils');

async function previewMarkdown(webviewPanel, context, newDocument = null) {
	try {
		let webview = webviewPanel.webView;

		var activeEditor = await hx.window.getActiveTextEditor();

		if (!activeEditor) return

		var document = activeEditor.document;

		if (newDocument) {
			document = newDocument;
		}

		if (document.languageId != 'markdown') {
			return;
		}

		const content = document.getText();

		const cssPath = `${context.extensionPath}/src/style/github-markdown.css`;

		const html = `
			<html>
			<head>
			</head>
			<body>
				<link rel="stylesheet" href="${cssPath}">
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/styles/default.min.css">
				<article class="markdown-body">
					${await markdown2html(content)}
				</article>
			<body>
			<script>
				function getContentHeight() {
				  var body = document.body;
				  var html = document.documentElement;
				
				  // 在标准模式下，使用documentElement
				  // 在混杂模式下，使用body
				  var height = Math.max(body.scrollHeight, html.scrollHeight);
				
				  return height;
				}
				window.onload = function () {
					document.addEventListener('click', function (event) {
						if (event.target && event.target.tagName == 'A') {
							const href = event.target.href;
							if (href.indexOf('http') > -1) {
								hbuilderx.postMessage({
									command: 'openWeb',
									href: href
								})
							}
							event.preventDefault();
						}
					})
				}
				
				function initReceive() {
					hbuilderx.onDidReceiveMessage((msg) => {
						if (msg.command === 'scroll') {
							window.scrollTo(0, getContentHeight() * msg.scrollRatio)
						}
					})
					
					hbuilderx.postMessage({
						command: 'init'
					})
				}
				
				window.addEventListener('hbuilderxReady', initReceive)
			</script>
			</html>
		`;

		webview.html = html;

		let isRead = false

		webview.onDidReceiveMessage((msg) => {
			if (msg.command == 'openWeb') {
				hx.env.openExternal(msg.href);
			}
			if (msg.command == 'init') {
				isRead = true
			}
		});

		let previousStartLine = null;

		const send = (data) => {
			// if (!isRead) {
			// 	console.log('页面初始化中，请稍后');
			// }

			webview.postMessage(data)
		}

		hx.window.onDidChangeTextEditorVisibleRanges(async event => {

			// 获取可见区域的起始行
			const visibleRange = event.visibleRanges[0]
			const currentStartLine = visibleRange.startLineNumber

			if (previousStartLine === null) {
				previousStartLine = currentStartLine
				return
			}

			if (event.textEditor.document.uri.path !== activeEditor.document.uri.path) return

			const linesScrolled = currentStartLine - previousStartLine
			const scrollRatio = currentStartLine / document.lineCount

			send({
				command: 'scroll',
				scrollRatio: scrollRatio
			})

			previousStartLine = visibleRange.startLineNumber
		})
	} catch (err) {
		console.error(err);
	}
}

async function showView() {
	var activeEditor = await hx.window.getActiveTextEditor();

	var document = activeEditor.document;

	if (document.languageId != 'markdown') {
		hx.window.showErrorMessage('文件格式错误，只支持markdown文件预览');
		return;
	}

	hx.window.showView({
		viewId: 'extension.kux.mdp.view.markdown',
		containerId: 'kux.mdp.viewContainer'
	});
}

module.exports = {
	previewMarkdown,
	showView
};