const md = require("./markdown");
const hx = require('hbuilderx')

function markdown2html (markdown) {
	try {
		return md.render(markdown);
	} catch (err) {
		console.error(err);
	}
}

async function getDocumentContentLineCount (document) {
	let contentLineCount = 0
	
	for (let i = 0; i < document.lineCount; i++) {
		const lineText = (await document.lineAt(i)).text
		contentLineCount = i
	}
	
	return contentLineCount
}

module.exports = {
	markdown2html,
	getDocumentContentLineCount
}